# University of California: Irvine's Hyperloop Team, HyperXite VI <dl>
The Hyperloop pod is a vehicle that is set to revolutionize the technological advancement
of transportation systems. Like the bullet train, it is meant to transverse from point A to B at a
tremendous speed, making it convenient for people that rely on transportation systems for
traveling and commuting. However, the Hyperloop pod is designed to travel through a vacuum
tube to negate air friction so that the pod can achieve high accelerations. Ideally, the concept
compensates the practical modes of transportation by being relatively inexpensive compared to
airfares and fast compared to public transportation methods. The HyperXite team has been
building scaled-down prototype pods for the past four years with this Hyperloop vision in mind.
After determining that building a full prototype pod would not be feasible for the team this year
given the state of the competition and budgetary constraints, the team decided to move forward
with a scaled down version of the pod with design concepts that we wanted to test.<dl>
The overarching objective of HyperXite is to design a scaled-down hyperloop pod to
participate in SpaceX’s annual competition. This competition is meant to explore the technology
used to accelerate the development of the pod, as well as understand the limitations and
challenges for generating a robust design for large-scale production. Due to the delay in the
Hyperloop competition, the goal is to research and develop new designs through an iterative
process. With this freedom to test new designs, we are striving to design a pod that can propel at
higher speeds, increase braking time, and manage power efficiently. <dl>
To learn more about our team, check out our website [here](https://www.hyperxite.com )
## Power Systems Subteam 
|Name | Positon | Email |
|---- | --------|--------------|
|[Nathan Bernardo](https://www.linkedin.com/in/nathan-bernardo-43217317b/)| Electronic Systems Engineer |ncbernard@uci.edu|
|[Beverly Ortiz-Ramirez](https://www.linkedin.com/in/beverlyortizramirez/)| Power Systems Engineer| bortizra@uci.edu |
|[Safia Reazi](https://www.linkedin.com/in/safiareazi/)| Power Systems Engineer| reazis@uci.edu |
## Purpose
Power’s objective is to implement and test an electrical system that is able to actuate
the mechanical components for braking, interface with the microcontrollers and computer during
operation, and provide power to motors to propel our pod. <dl>
Extensive research was conducted on various Printed Circuit Board (PCB) designs prior
to designing the one for our systems. In addition, we evaluated older versions of the PCB design
from previous HyperXite Pod’s, noting ways in which it could be improved.The following PCB was designed through Altium. 
## Images
Downloadable Altium files can be found [here](https://gitlab.com/Geoff_2020/hyperxite_vl_pcb/-/tree/master/Final%20Versions)<dl>
![alt text](README.md.references/Flow_Chart.PNG "Image of FC")<dl>
<dl> Figure 1: PCB Flow Chart <dl>
![alt text](README.md.references/Schematic.PNG "Image of PCB Schematic")<dl>
<dl> Figure 2: Schematic PCB <dl>
![alt text](README.md.references/2D.PNG "Image of 2D PCB")<dl>
<dl> Figure 3: 2D render of PCB <dl>
![alt text](README.md.references/3D.PNG "Image of 3D PCB")<dl>
<dl> Figure 4: 3D render of PCB <dl>
![alt text](README.md.references/Manufactured.PNG "Image of manufactured PCB")<dl>
<dl> Figure 5: Manufactured PCB from JLCPCB <dl>
![alt text](README.md.references/Soldering_Progress.PNG "Image of Soldered PCB")<dl>
<dl> Figure 6: Soldering Progress of PCB as of November 2020 <dl>

## Lab Clips
![Safia Reazi and Beverly Ortiz-Ramirez Soldering ](README.md.references/Lab_Clip.mp4)







